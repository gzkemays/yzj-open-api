package cn.yzj.openapi.config.redis;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import redis.clients.jedis.JedisPool;
import redis.clients.jedis.JedisPoolConfig;

import java.time.Duration;

/**
 * Redis 配置类
 *
 * @author gzkemays
 * @since 2021/12/22 19:46
 */
@Configuration
@EnableCaching
public class RedisConfiguration {
  @Value("${spring.cache.redis.time-to-live:60000}")
  private Duration timeToLive;

  @Value("${spring.redis.host}")
  private String host;

  @Value("${spring.redis.port}")
  private int port;

  @Value("${spring.redis.timeout}")
  private int timeout;

  @Value("${spring.redis.password}")
  private String password;

  public Jackson2JsonRedisSerializer<Object> jsonRedisSerializer() {
    Jackson2JsonRedisSerializer<Object> serializer =
        new Jackson2JsonRedisSerializer<>(Object.class);
    ObjectMapper objectMapper = new ObjectMapper();
    // 指定要序列化的域，field,get和set,以及修饰符范围，ANY是都有包括private和public
    objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
    // 指定序列化输入的类型，类必须是非final修饰的，final修饰的类，比如String,Integer等会跑出异常
    objectMapper.activateDefaultTyping(
        LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL);
    serializer.setObjectMapper(objectMapper);

    return serializer;
  }

  @Bean
  public RedisTemplate<String, Object> redisTemplate(
      RedisConnectionFactory redisConnectionFactory) {
    RedisTemplate<String, Object> template = new RedisTemplate<>();
    template.setConnectionFactory(redisConnectionFactory);
    StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();

    template.setKeySerializer(stringRedisSerializer);
    template.setValueSerializer(jsonRedisSerializer());

    template.setHashKeySerializer(stringRedisSerializer);
    template.setHashValueSerializer(jsonRedisSerializer());

    template.afterPropertiesSet();
    return template;
  }

  @Bean
  @Primary
  public CacheManager cacheManager(RedisConnectionFactory redisConnectionFactory) {
    StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
    // 配置序列化（解决乱码的问题）
    RedisCacheConfiguration config =
        RedisCacheConfiguration.defaultCacheConfig()
            // 缓存有效期
            .entryTtl(timeToLive)
            // 使用StringRedisSerializer来序列化和反序列化redis的key值
            .serializeKeysWith(
                RedisSerializationContext.SerializationPair.fromSerializer(stringRedisSerializer))
            // 使用Jackson2JsonRedisSerializer来序列化和反序列化redis的value值
            .serializeValuesWith(
                RedisSerializationContext.SerializationPair.fromSerializer(jsonRedisSerializer()))
            // 禁用空值
            .disableCachingNullValues();

    return RedisCacheManager.builder(redisConnectionFactory).cacheDefaults(config).build();
  }

  @Bean
  public JedisPool jedisPoolFactory() {
    JedisPoolConfig jedisPoolConfig = new JedisPoolConfig();
    jedisPoolConfig.setMaxIdle(10);
    // 连接耗尽时是否阻塞, false报异常,true阻塞直到超时, 默认true
    jedisPoolConfig.setBlockWhenExhausted(false);
    // 是否启用pool的jmx管理功能, 默认true
    //    jedisPoolConfig.setJmxEnabled(JmxEnabled);
    return new JedisPool(jedisPoolConfig, host, port, timeout, password);
  }
}
