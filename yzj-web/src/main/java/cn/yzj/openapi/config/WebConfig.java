package cn.yzj.openapi.config;

import cn.yzj.openapi.interceptor.ShortUrlInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * web 配置
 *
 * @author gzkemays
 * @since 2021/10/25 17:28
 */
@Configuration
public class WebConfig extends WebMvcConfigurationSupport {

  @Bean
  public ShortUrlInterceptor shortUrlInterceptor() {
    return new ShortUrlInterceptor();
  }

  @Override
  protected void addCorsMappings(CorsRegistry registry) {
    registry.addMapping("/**").allowedHeaders("*").allowedMethods("*").allowedOrigins("*");
  }

  @Override
  protected void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
    configurer.defaultContentType(MediaType.APPLICATION_JSON);
    //    super.configureContentNegotiation(configurer);
  }

  @Override
  protected void addInterceptors(InterceptorRegistry registry) {
    //    registry.addInterceptor(shortUrlInterceptor());
    //    super.addInterceptors(registry);
  }
}
