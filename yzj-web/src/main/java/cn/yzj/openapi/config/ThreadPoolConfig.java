package cn.yzj.openapi.config;

import cn.yzj.utils.ThreadPoolConfigUtils;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * 线程池配置
 *
 * @author gzkemays
 * @since 2022/6/1 8:08
 */
@Configuration
public class ThreadPoolConfig {
  @Bean(name = "threadPoolExecutor")
  public ThreadPoolExecutor threadPoolTaskExecutor() {
    return ThreadPoolConfigUtils.builder()
        .createThreadPool()
        .corePoolSize(4)
        .maximumPoolSize(8)
        .openAllowCoreThread(10, TimeUnit.MINUTES)
        .build()
        .getThreadPoolExecutor();
  }
}
