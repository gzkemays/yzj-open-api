package cn.yzj.openapi.config.cache;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

/**
 * 缓存配置
 *
 * @author gzkemays
 * @since 2022/3/30 17:43
 */
@Configuration
public class CacheConfig {
  @Bean(name = "settingCache")
  public Cache<String, Object> settingCache() {
    return CacheBuilder.newBuilder().initialCapacity(2).maximumSize(5).build();
  }

  @Bean(name = "epidemicCache")
  public Cache<String, Object> epidemicCache() {
    return CacheBuilder.newBuilder()
        .expireAfterWrite(30, TimeUnit.MINUTES)
        .initialCapacity(5)
        .maximumSize(10)
        .build();
  }

  @Bean(name = "emailCache")
  public Cache<String, Object> emailCache() {
    return CacheBuilder.newBuilder()
        .expireAfterWrite(1, TimeUnit.MINUTES)
        .initialCapacity(100)
        .maximumSize(500)
        .build();
  }

  @Bean(name = "tiebaCache")
  public Cache<String, String> tiebaCache() {
    return CacheBuilder.newBuilder()
        .expireAfterWrite(1, TimeUnit.DAYS)
        .initialCapacity(50)
        .maximumSize(100)
        .build();
  }

  @Bean(name = "sfCache")
  public Cache<String, String> sfCache() {
    return CacheBuilder.newBuilder().initialCapacity(5).maximumSize(10).build();
  }
}
