package cn.yzj.openapi.task;

import cn.yzj.openapi.service.SignService;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import javax.annotation.Resource;

/**
 * 签到任务
 *
 * @author gzkemays
 * @since 2022/4/28 23:50
 */
@Configuration
@EnableScheduling
public class SignTask {
  @Resource SignService signService;
  /** 贴吧打卡 */
  @Scheduled(cron = "0 0 7 * * ?")
  private void tiebaSign() {
    signService.tiebaSign();
  }
  /** xiaoaishe 打卡 */
  @Scheduled(cron = "0 0 12 * * ?")
  private void fuliSign() {
    signService.fuliSign();
  }

  @Scheduled(cron = "5 0 0 * * ?")
  private void sfSign() {
    signService.sfSign();
  }
}
