package cn.yzj.openapi.controller;

import cn.yzj.openapi.annotation.OpenApiAuth;
import cn.yzj.openapi.common.FinalResult;
import cn.yzj.openapi.service.RandomImgService;
import cn.yzj.openapi.utils.ReturnResultUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;

/**
 * 随机图控制器
 *
 * @author gzkemays
 * @since 2022/3/30 18:15
 */
@RestController
@RequestMapping("random")
public class RandomImgController {
  @Resource RandomImgService imgService;

  @OpenApiAuth(security = "gzkemays")
  @PostMapping("upload")
  public FinalResult imgUpload(MultipartFile[] files) {
    return ReturnResultUtils.simple(imgService.upload(files));
  }
}
