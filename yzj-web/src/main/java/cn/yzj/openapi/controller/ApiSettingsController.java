package cn.yzj.openapi.controller;

import cn.yzj.openapi.annotation.OpenApiAuth;
import cn.yzj.openapi.common.FinalResult;
import cn.yzj.openapi.converter.SettingsConverter;
import cn.yzj.openapi.dao.intf.ApiSettingsDao;
import cn.yzj.openapi.dto.SettingsDTO;
import cn.yzj.openapi.utils.ReturnResultUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * 配置控制器
 *
 * @author gzkemays
 * @since 2022/3/30 18:32
 */
@RestController()
@RequestMapping("/settings")
public class ApiSettingsController {
  @Resource ApiSettingsDao dao;

  @PostMapping("/update")
  public FinalResult update(@Valid SettingsDTO dto) {
    dao.update(SettingsConverter.INSTANCE.convertEntityByDTO(dto));
    return ReturnResultUtils.simple("ok");
  }

  @PostMapping("/insert")
  @OpenApiAuth(security = "gzkemays_settings")
  public FinalResult insert(@Valid @RequestBody SettingsDTO dto) {
    dao.insertSettings(dto);
    return ReturnResultUtils.simple("ok");
  }
}
