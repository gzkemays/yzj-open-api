package cn.yzj.openapi.controller;

import cn.yzj.openapi.common.Common;
import cn.yzj.openapi.dao.intf.ShortUrlDao;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 地址重定向控制器
 *
 * @author gzkemays
 * @since 2021/11/12 0:53
 */
@RestController
@RequestMapping("/s")
public class UrlRedirectController {
  @Resource ShortUrlDao urlDao;

  @GetMapping("/{key}")
  public void redirect(@PathVariable String key) {
    urlDao.getOriginUrl(Common.SHORT_URL_PREFIX + key);
  }
}
