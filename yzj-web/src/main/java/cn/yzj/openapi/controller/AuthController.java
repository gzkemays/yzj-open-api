package cn.yzj.openapi.controller;

import cn.yzj.openapi.annotation.OpenApiAuth;
import cn.yzj.openapi.common.FinalResult;
import cn.yzj.openapi.dao.intf.TiebaSignDao;
import cn.yzj.openapi.dto.EmailDTO;
import cn.yzj.openapi.service.EmailService;
import cn.yzj.openapi.service.SignService;
import cn.yzj.openapi.utils.ReturnResultUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;

/**
 * @author gzkemays
 * @since 2022/4/15 17:48
 */
@RestController
@RequestMapping("/auth")
public class AuthController {
  @Resource EmailService emailService;
  @Resource TiebaSignDao signDao;
  @Resource SignService signService;

  @OpenApiAuth(security = "gzkemays")
  @PostMapping(path = "/email")
  public FinalResult sendEmailCode(@Valid @RequestBody EmailDTO dto) {
    return ReturnResultUtils.simple(emailService.sendVerificationCode(dto));
  }

  @OpenApiAuth(security = "yzjgroup")
  @PostMapping(path = "/groupEmail")
  public FinalResult sendEmailText(@Valid EmailDTO dto) {
    emailService.send(dto);
    return ReturnResultUtils.simple(0);
  }

  @OpenApiAuth(security = "yzjgroup")
  @PostMapping(path = "/groupMail")
  public FinalResult groupSendEmail(@Valid @RequestBody EmailDTO dto) {
    emailService.send(dto);
    return ReturnResultUtils.simple(0);
  }

  @OpenApiAuth(security = "gzkemays-tieba")
  @PostMapping(path = "/tieba/signAll")
  public FinalResult handToSign() {
    signDao.sign();
    return ReturnResultUtils.simple(null);
  }

  @OpenApiAuth(security = "gzkemays-tieba")
  @PostMapping(path = "/tieba/sign")
  public FinalResult handToSign(String username) {
    return ReturnResultUtils.simple(signDao.sign(username));
  }

  @OpenApiAuth(security = "gzkemays-xiaoaishe")
  @GetMapping(path = "/xiaoaishe/sign")
  public FinalResult fuliSign() {
    signService.fuliSign();
    return ReturnResultUtils.simple("OK");
  }

  @OpenApiAuth(security = "gzkemays-sifu")
  @GetMapping(path = "/sf/sign")
  public FinalResult sfSign() {
    signService.sfSign();
    return ReturnResultUtils.simple(null);
  }
}
