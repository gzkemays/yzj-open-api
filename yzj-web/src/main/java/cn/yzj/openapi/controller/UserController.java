package cn.yzj.openapi.controller;

import cn.yzj.openapi.annotation.OpenApiAuth;
import cn.yzj.openapi.common.FinalResult;
import cn.yzj.openapi.dao.intf.ApiUserDao;
import cn.yzj.openapi.utils.ReturnResultUtils;
import com.alibaba.fastjson.JSONObject;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 用户控制器
 *
 * @author gzkemays
 * @since 2022/6/24 9:15
 */
@RestController
@RequestMapping("/user")
public class UserController {
  @Resource ApiUserDao userDao;
  /**
   * 导航云存根
   *
   * @return 响应json
   */
  @OpenApiAuth(header = HttpHeaders.AUTHORIZATION)
  @PostMapping("/navigate/cloudsave")
  public FinalResult navigateCloudSave(@RequestBody Map<String, Object> navigate) {
    return ReturnResultUtils.simple(userDao.cloudConfigSave(new JSONObject(navigate)));
  }

  @GetMapping("/{refer}/{username}")
  public FinalResult getNavigateConfig(
      @PathVariable(name = "refer") String refer,
      @PathVariable(name = "username") String username) {
    return ReturnResultUtils.simple(userDao.getCloudConfig(refer, username));
  }
}
