package cn.yzj.openapi.controller;

import cn.yzj.openapi.common.FinalResult;
import cn.yzj.openapi.common.ResultCode;
import cn.yzj.openapi.dao.intf.DomainIconDao;
import cn.yzj.openapi.dto.DomainIconDTO;
import cn.yzj.openapi.exception.OpenApiException;
import cn.yzj.openapi.utils.ReturnResultUtils;
import cn.yzj.openapi.vo.DomainIconVO;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 网站图标
 *
 * @author gzkemays
 * @since 2022/6/19 17:54
 */
@RestController
@RequestMapping("/icon")
public class DomainIconController {
  @Resource DomainIconDao domainIconDao;

  @GetMapping("/{domain}")
  public FinalResult getIconByDomainGet(@PathVariable("domain") String domain) {
    return ReturnResultUtils.simple(
        new DomainIconVO(domainIconDao.getIconByDomain(new DomainIconDTO(domain))));
  }

  @PostMapping("/domain")
  public FinalResult getIconByDomainPost(@RequestBody Map<String, String> domain) {
    if (!domain.containsKey("domain")) {
      throw new OpenApiException(ResultCode.DATA_ERROR, "domain 不能为空");
    }
    return ReturnResultUtils.simple(
        new DomainIconVO(
            domainIconDao.getIconByDomainByHttp(new DomainIconDTO(domain.get("domain")))));
  }

  @PostMapping("/domains")
  public FinalResult getIconsByDomains(@RequestBody List<String> domains) {
    return ReturnResultUtils.simple(domainIconDao.getDomainIconList(domains));
  }
}
