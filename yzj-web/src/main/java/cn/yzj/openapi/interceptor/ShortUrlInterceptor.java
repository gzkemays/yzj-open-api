package cn.yzj.openapi.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 短链接拦截器
 *
 * @author gzkemays
 * @since 2021/11/11 23:29
 */
@Component
@Slf4j
public class ShortUrlInterceptor extends HandlerInterceptorAdapter {

  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {
    log.info("请求地址 --> {}", request.getRequestURL());
    log.info("请求服务 --> {}", request.getRequestURI());
    return super.preHandle(request, response, handler);
  }
}
