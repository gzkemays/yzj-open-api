package cn.yzj.openapi;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author gzkemays
 */
@SpringBootApplication
@ComponentScan(
    basePackages = {
      "cn.yzj.openapi.dao",
      "cn.yzj.openapi.utils",
      "cn.yzj.openapi.config",
      "cn.yzj.openapi.controller",
      "cn.yzj.openapi.service",
      "cn.yzj.openapi.exception",
      "cn.yzj.openapi.common",
      "cn.yzj.openapi.basic",
      "cn.yzj.openapi.aspect",
      "cn.yzj.openapi.annotation",
      "cn.yzj.openapi.task"
    })
@MapperScan({"cn.yzj.openapi.mapper"})
public class YzjOpenApiApplication {

  public static void main(String[] args) {
    SpringApplication.run(YzjOpenApiApplication.class, args);
  }
}
