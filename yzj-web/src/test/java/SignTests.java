import cn.yzj.http.YzjHttp;
import cn.yzj.http.YzjHttpResult;
import cn.yzj.openapi.YzjOpenApiApplication;
import cn.yzj.openapi.basic.WebBasicAuthRequest;
import cn.yzj.openapi.bo.SignSettingsBO;
import cn.yzj.openapi.service.SignService;
import cn.yzj.openapi.service.impl.SignServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @author gzkemays
 * @since 10/18/2022
 */
@SpringBootTest(classes = YzjOpenApiApplication.class)
@Slf4j
public class SignTests {
  @Resource SignServiceImpl signServiceImpl;
  @Resource SignService signService;

  @Test
  void test() {
    SignSettingsBO signSettingsBO = new SignSettingsBO();
    signSettingsBO.setSign(true);
    signSettingsBO.setBaseUrl("hejiba.vip");
    signSettingsBO.setUsername("1538211287@qq.com");
    signSettingsBO.setPassword("a96548854");
    WebBasicAuthRequest login = signServiceImpl.login(signSettingsBO);
    SignService.FuliBasicUrls urls = new SignService.FuliBasicUrls(signSettingsBO.getBaseUrl());
    Map<String, String> headers = signServiceImpl.getHeaders(login, urls);
    System.out.println("cookie = " + headers.get("Cookie"));
    System.out.println("token = " + headers.get("Authorization"));
    /*    Map<String, String> headers = new HashMap<>();
    headers.put("Authorization", login.getToken());*/
    System.out.println("headers = " + headers);
    YzjHttpResult newDmsgResult =
        YzjHttp.createPost("https://hejiba.vip/wp-json/b2/v1/getTaskData")
            .headers(headers)
            .defaultUserAgent()
            .send();
    System.out.println("newDmsgResult = " + newDmsgResult);
  }

  @Test
  void service() {
    signService.fuliSign();
  }
}
