import cn.yzj.http.YzjHttp;
import cn.yzj.http.YzjHttpResult;
import cn.yzj.http.YzjSendConfig;
import cn.yzj.openapi.YzjOpenApiApplication;
import cn.yzj.openapi.basic.WebBasicAuthRequest;
import cn.yzj.openapi.service.SignService;
import cn.yzj.utils.JsonUtils;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author gzkemays
 * @date 2022/8/2
 */
@SpringBootTest(classes = YzjOpenApiApplication.class)
@Slf4j
public class XiaoAiTests {
  @Resource SignService signService;
  final Pattern DOC_JSON_PATTERN = Pattern.compile("\\{([^}]*)}");
  static List<String> refreshTestsUrl = new ArrayList<>();

  static {
    // Post: ref = null
    refreshTestsUrl.add("https://xiaoai986.com/wp-json/b2/v1/getUserInfo");
    // Post: count = 3;
    refreshTestsUrl.add("https://xiaoai986.com/wp-json/b2/v1/getLatestAnnouncement");
    refreshTestsUrl.add("https://xiaoai986.com/wp-json/b2/v1/getTaskData");
    refreshTestsUrl.add("https://xiaoai986.com/wp-json/b2/v1/tjuser");
    refreshTestsUrl.add("https://xiaoai986.com/wp-json/b2/v1/getNewDmsg");
    // Post: count:3, paged: 1;
    refreshTestsUrl.add("https://xiaoai986.com/wp-json/b2/v1/getUserMission");
  }

  @Test
  void sign() {
    signService.fuliSign();
  }

  @Test
  void testRefresh() {
    Map<String, String> headers = getHeaders();
    Map<String, Object> data = new HashMap<>();
    data.put("count", 3);
    data.put("paged", 1);
    YzjHttpResult send =
        YzjHttp.createPost("https://xiaoai986.com/wp-json/b2/v1/getUserMission")
            .headers(headers)
            .dataMap(data)
            .defaultUserAgent()
            .send();
    log.info("userMission -> {}", send.getResult());
    xiaoAiSheMsgRefresh(headers);
  }

  @Test
  void test() {
    Map<String, String> headers = getHeaders();
    xiaoAiSheMsgRefresh(headers);
  }

  public Map<String, String> getHeaders() {
    WebBasicAuthRequest webBasicAuthRequest = xiaoAiSheLogin();
    Map<String, String> headers = new HashMap<>();
    headers.put("Cookie", webBasicAuthRequest.getCookie());
    headers.put("Authorization", webBasicAuthRequest.getToken());
    headers.put("Host", "xiaoai986.com");
    headers.put("Referer", "https://xiaoai986.com/task");
    headers.put("Origin", "https://xiaoai986.com");
    return headers;
  }

  public boolean xiaoAiSheMsgRefresh(Map<String, String> headers) {
    log.info("刷新 msg 判断是否有签到");
    YzjHttpResult newDmsgResult =
        YzjHttp.createPost("https://xiaoai986.com/wp-json/b2/v1/getNewDmsg")
            .headers(headers)
            .defaultUserAgent()
            .send();
    if (JsonUtils.isJson(newDmsgResult.getResult())) {
      YzjHttpResult send =
          YzjHttp.createPost("https://xiaoai986.com/wp-json/b2/v1/getTaskData")
              .headers(headers)
              .defaultUserAgent()
              .send();
      String result = send.getResult();
      log.info("refresh msg = {}", result);
      if (JsonUtils.isJson(result)) {
        JSONObject json = JSONObject.parseObject(result);
        JSONObject task = json.getJSONObject("task");
        int finish =
            Optional.ofNullable(task.getJSONObject("task_mission").get("finish"))
                .map(i -> (int) i)
                .orElse(1);
        if (finish == 1) {
          log.warn("签到未刷新 -> {}", task.getJSONObject("task_mission"));
        }
        return finish != 1;
      }
      return JsonUtils.isJson(send.getResult());
    }
    return false;
  }

  public WebBasicAuthRequest xiaoAiSheLogin() {
    WebBasicAuthRequest webBasicAuthRequest = new WebBasicAuthRequest();
    String tmpToken = "https://xiaoai986.com/wp-json/b2/v1/getRecaptcha";
    String login = "https://xiaoai986.com/wp-json/jwt-auth/v1/token";
    YzjHttpResult send = YzjHttp.createPost(tmpToken).defaultUserAgent().send();
    String result = send.getResult();
    Matcher m = DOC_JSON_PATTERN.matcher(result);
    if (m.find()) {
      String json = m.group();
      String token = JSON.parseObject(json).getString("token");
      Map<String, Object> map = new HashMap<>(3);
      map.put("username", "gzkemays");
      map.put("password", "a96548854");
      map.put("token", token);
      YzjHttpResult loginSend =
          YzjHttp.createPost(login)
              .defaultUserAgent()
              .dataMap(map)
              .config(YzjSendConfig.start().cookie().postJson().end())
              .send();
      String cookie = loginSend.getCookie();
      webBasicAuthRequest.setToken(cookie.replace("b2_token=", "Bearer ").replace(";", ""));
      webBasicAuthRequest.setCookie(cookie);
    }
    return webBasicAuthRequest;
  }

  public static void main(String[] args) {

    Map<String, String> newHeaders = new HashMap<>();
    newHeaders.put(
        "Authorization",
        "Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwczpcL1wvd3d3Lnp0ZW1hbi5uZXQiLCJpYXQiOjE2NjYwNzI5NjksIm5iZiI6MTY2NjA3Mjk2OSwiZXhwIjoxNjY3MjgyNTY5LCJkYXRhIjp7InVzZXIiOnsiaWQiOiIyNjk4MiJ9fX0.VX10XYvCY-UGKvKgsZqtoCbI1PKUhSo8gSApYzAK2-k"
            .trim());
    YzjHttpResult newDmsgResult =
        YzjHttp.createPost("https://www.zteman.net/wp-json/b2/v1/getTaskData")
            .headers(newHeaders)
            .defaultUserAgent()
            .send();
    System.out.println("newDmsgResult = " + newDmsgResult);
  }
}
