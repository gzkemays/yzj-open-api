import cn.yzj.http.YzjHttp;
import cn.yzj.http.YzjHttpResult;
import cn.yzj.openapi.YzjOpenApiApplication;
import cn.yzj.openapi.bo.SignSettingsBO;
import cn.yzj.openapi.dao.intf.ApiSettingsDao;
import cn.yzj.openapi.dao.intf.DomainIconDao;
import cn.yzj.openapi.dao.intf.ShortUrlDao;
import cn.yzj.openapi.dao.intf.TiebaSignDao;
import cn.yzj.openapi.dto.UrlDTO;
import cn.yzj.openapi.entity.ApiSettingsEntity;
import cn.yzj.openapi.service.EpidemicService;
import cn.yzj.openapi.service.SignService;
import cn.yzj.openapi.vo.EpidemicVO;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import com.google.common.cache.Cache;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author gzkemays
 * @since 2022/1/14 21:42
 */
@SpringBootTest(classes = YzjOpenApiApplication.class)
public class Tests {
  @Resource ShortUrlDao urlDao;
  @Resource ApiSettingsDao settingsDao;
  @Resource EpidemicService epidemicService;
  @Resource TiebaSignDao signDao;
  @Resource SignService signService;
  @Resource DomainIconDao iconDao;
  @Resource JedisPool jedisPool;

  @Resource(name = "tiebaCache")
  Cache<String, String> cache;

  public static void main(String[] args) throws IOException {

    String txt = "\u8bf7\u5148\u767b\u5f55";
    System.out.println("txt = " + txt);
  }

  @Test
  public void sfTests() {
    String url = "https://www.jiaosf.com/plugin.php";
    Map<String, String> headers = new HashMap<>();
    Map<String, Object> data = new HashMap<>();
    headers.put("referer", "https://www.jiaosf.com/");
    headers.put(
        "Cookie",
        "oz1H_2132_lastvisit=1660005802; oz1H_2132_saltkey=f4zA9YaR; oz1H_2132_auth=9fdamHu9IO34cIWVNXDo0PjRH2cEBChG4qnxfaD25qboFOmekJ3KVeUmTqmUr49Z6Ux%2FDTlnPP0FoCz7V5y2jbm6nQ; oz1H_2132_lastcheckfeed=40110%7C1660009415; oz1H_2132_connect_is_bind=0; oz1H_2132_nofavfid=1; oz1H_2132_smile=1D1; oz1H_2132_visitedfid=125D278D280D159; oz1H_2132_fastpostrefresh=1; oz1H_2132_forum_lastvisit=D_159_1660287863D_280_1660297873D_278_1660297920D_125_1660317861; oz1H_2132_sid=u1XGSH; oz1H_2132_lip=183.22.33.207%2C1660317837; oz1H_2132_ulastactivity=73d88hbml%2FNmsl7JlIQ7g2MUN0LzbpaBgouAA10tEv7nTxi3R5i4; oz1H_2132_checkpm=1; oz1H_2132_sendmail=1; oz1H_2132_noticeTitle=1; Hm_lvt_b755d2bffc75fda64e277513a4498c4f=1660295026,1660301725,1660317838,1660376123; oz1H_2132_lastact=1660376135%09plugin.php%09; Hm_lpvt_b755d2bffc75fda64e277513a4498c4f=1660376136");
    headers.put("Accept", "*/*");
    data.put("id", "k_misign:sign");
    data.put("operation", "qiandao");
    data.put("format", "button");
    data.put("formhash", "57e4e307");
    data.put("inajax", 1);
    data.put("ajaxtarget", "midaben_sign");
    YzjHttpResult send = YzjHttp.createGet(url).headers(headers).dataMap(data).send();
    System.out.println(send.getResult());
  }

  @Test
  public void jedisTests() {
    Jackson2JsonRedisSerializer<Object> serializer =
        new Jackson2JsonRedisSerializer<>(Object.class);
    ObjectMapper objectMapper = new ObjectMapper();
    // 指定要序列化的域，field,get和set,以及修饰符范围，ANY是都有包括private和public
    objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
    // 指定序列化输入的类型，类必须是非final修饰的，final修饰的类，比如String,Integer等会跑出异常
    objectMapper.activateDefaultTyping(
        LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL);
    serializer.setObjectMapper(objectMapper);
    Jedis resource = jedisPool.getResource();
  }

  @Test
  public void test2() {
    signDao.sign();
  }

  @Test
  public void domainIcon() {
    List<String> getToHttpIcon = iconDao.createGetToHttpIcon("https://xiaoai986.com/");
    System.out.println(getToHttpIcon);
    //    List<String> strs = new ArrayList<>();
    //    strs.add("https://www.primefaces.org/");
    //    System.out.println(iconDao.getDomainIconList(strs));
    //

  }

  @Test
  public void sign() {
    signService.fuliSign();
    /*    Map<String, Object> map = new HashMap<>(3);
    map.put("username", "1538211287@qq.com");
    map.put("password", "a96548854");
    YzjHttpResult loginSend =
        YzjHttp.createPost("https://www.zteman.net/wp-json/jwt-auth/v1/token")
            .defaultUserAgent()
            .dataMap(map)
            .config(YzjSendConfig.start().cookie().postJson().end())
            .send();
    String cookie = loginSend.getCookie();
    System.out.println("cookie = " + cookie);
    int first = cookie.indexOf(";");
    String token = cookie.substring(0, first).replace("b2_token=", "Bearer ");
    System.out.println("token = " + token);*/
    //    signService.sfSign();
  }

  @Test
  public void epidemic() {
    List<EpidemicVO> gz = this.epidemicService.getEpidemic("广州");
    System.out.println(gz);
  }

  @Test
  public void test() {
    UrlDTO shortUrlDTO = new UrlDTO();
    shortUrlDTO.setUrl(
        "https://www.qcc.com/web/search?key=%E5%B9%BF%E5%B7%9E%E5%B8%82%E5%A4%A9%E6%B2%B3%E5%8C%BA%E9%AB%98%E6%96%B0%E6%8A%80%E6%9C%AF%E4%BA%A7%E4%B8%9A%E5%BC%80%E5%8F%91%E5%8C%BA%E5%B7%A5%E4%B8%9A%E5%9B%AD%E5%BB%BA%E4%B8%AD%E8%B7%AF62%E5%8F%B72%E6%A5%BC%E5%8C%97%E5%8C%BA210%E3%80%81212%E5%8F%B7%E6%88%BF%E5%B1%8B");
    System.out.println(urlDao.generateShortUrl(shortUrlDTO));
    //    urlDao.getOriginUrl("http://open.yeziji.xyz/s/6JtyxT");
  }

  @Test
  public void settings() {
    ApiSettingsEntity sign = settingsDao.selectById("SIGN");
    String settings = sign.getSettings();
    System.out.println(SignSettingsBO.buildList(sign));
  }
}
