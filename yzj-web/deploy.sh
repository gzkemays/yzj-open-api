docker_project='openapi';
echo "--- 停止容器 ---"
docker stop ${docker_project};
echo "--- 删除容器 ---"
docker rm ${docker_project};
echo "--- 删除镜像 ---"
docker rmi ${docker_project};
echo "--- 构造镜像 ---"
docker build -t ${docker_project} .;
echo "--- 构造容器 ---"
docker run -d --name ${docker_project} -p 8880:8880 ${docker_project};
# echo "--- 开始连接至公共网关 ---"
# docker network connect common ${docker_project}
# echo "--- 连接网关完毕 ---"
