package cn.yzj.openapi.annotation;

import java.lang.annotation.*;

/**
 * 开启api权限校验注解
 *
 * @author gzkemays
 * @since 2022/4/13 11:40
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Inherited
public @interface OpenApiAuth {
  String header() default "auth";
  String security() default "";
}
