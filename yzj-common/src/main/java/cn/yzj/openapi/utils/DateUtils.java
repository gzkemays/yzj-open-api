package cn.yzj.openapi.utils;

import java.util.Calendar;
import java.util.Date;

/**
 * 时间工具类
 *
 * @author gzkemays
 * @since 2021/11/12 1:14
 */
public class DateUtils {
  /**
   * 获取 day 天后的时间戳
   *
   * @param day 天数
   * @return day 天后的时间戳
   */
  public static Date getDayBeforeTimeStamp(int day) {
    Calendar calendar = Calendar.getInstance();
    calendar.add(Calendar.DATE, +day);
    return calendar.getTime();
  }
}
