package cn.yzj.openapi.utils;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.jsontype.impl.LaissezFaireSubTypeValidator;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.stereotype.Component;
import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

import javax.annotation.Resource;

/**
 * Jedis 工具类
 *
 * @author gzkemays
 * @since 2022/6/24 9:10
 */
@Component
public class JedisUtils {
  @Resource JedisPool jedisPool;
  Jackson2JsonRedisSerializer<Object> serializer = jsonRedisSerializer();

  public Jackson2JsonRedisSerializer<Object> jsonRedisSerializer() {
    Jackson2JsonRedisSerializer<Object> serializer =
        new Jackson2JsonRedisSerializer<>(Object.class);
    ObjectMapper objectMapper = new ObjectMapper();
    // 指定要序列化的域，field,get和set,以及修饰符范围，ANY是都有包括private和public
    objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
    // 指定序列化输入的类型，类必须是非final修饰的，final修饰的类，比如String,Integer等会跑出异常
    objectMapper.activateDefaultTyping(
        LaissezFaireSubTypeValidator.instance, ObjectMapper.DefaultTyping.NON_FINAL);
    serializer.setObjectMapper(objectMapper);
    return serializer;
  }

  public void set(String key, Object value) {
    try (Jedis jedis = jedisPool.getResource()) {
      jedis.set(key.getBytes(), serializer.serialize(value));
    }
  }

  public void set(String key, Object value, int expired) {
    try (Jedis jedis = jedisPool.getResource()) {
      jedis.set(key.getBytes(), serializer.serialize(value));
      jedis.expire(key.getBytes(), expired);
    }
  }

  public void remove(String key) {
    try (Jedis jedis = jedisPool.getResource()) {
      jedis.del(key.getBytes());
    }
  }

  public Object get(String key) {
    try (Jedis jedis = jedisPool.getResource()) {
      return serializer.deserialize(jedis.get(key.getBytes()));
    }
  }
}
