package cn.yzj.openapi.utils;

import cn.yzj.common.CommonSymbol;
import cn.yzj.openapi.common.FinalResult;
import cn.yzj.openapi.common.ResultCode;
import cn.yzj.openapi.exception.OpenApiException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.ArrayList;
import java.util.List;

/**
 * 返回工具类
 *
 * @author gzkemays
 * @since 2021/11/11 23:26
 */
public class ReturnResultUtils {
  /**
   * 简单返回
   *
   * <p>固定 code 为 0，msg 为 ok
   *
   * @param data 返回数据
   * @return 返回结果
   */
  public static FinalResult simple(Object data) {
    return FinalResult.builder().code(0).msg("ok").data(data).build();
  }

  /**
   * openApi异常返回
   *
   * @param exception 指定异常
   * @return 返回结果
   */
  public static FinalResult exception(OpenApiException exception) {
    return FinalResult.builder()
        .code(exception.getCode())
        .msg(exception.getMsg())
        .data(null)
        .build();
  }

  public static FinalResult validate(BindingResult result) {
    List<FieldError> fieldErrors = result.getFieldErrors();
    List<String> msg = new ArrayList<>();
    for (FieldError fieldError : fieldErrors) {
      msg.add(fieldError.getDefaultMessage());
    }
    return FinalResult.builder()
        .code(ResultCode.ARGS_ERROR)
        .msg(StrUtils.listToStr(msg, CommonSymbol.COMMA))
        .build();
  }
}
