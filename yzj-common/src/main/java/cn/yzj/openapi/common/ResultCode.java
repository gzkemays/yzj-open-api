package cn.yzj.openapi.common;

/**
 * 返回结果码
 *
 * @author gzkemays
 * @since 2021/11/12 0:12
 */
public interface ResultCode {
  // 系统常见异常
  Integer SUCCESS = 0;
  Integer REQUEST_FAIL = -1;
  Integer METHOD_ERROR = -2;
  Integer DATA_ERROR = -3;
  Integer HTTP_ERROR = -4;
  Integer SQL_ERROR = -5;
  Integer SQL_OPERA_ERROR = -6;
  Integer UNKNOWN_ERROR = -999;
  Integer NULL_POINT_ERROR = -7;
  Integer ARGS_ERROR = -8;
  Integer SECURITY_ERROR = -9;
  Integer REDIRECT_URL_FAIL = -10;
  // 短链接异常
  Integer SHORT_URL_EMPTY_ERROR = -101;
  Integer SHORT_URL_CHECK_ERROR = -102;
  Integer SHORT_URL_TIME_OVER_ERROR = -103;
  Integer SHORT_URL_CLOSED_ERROR = -104;
  Integer SHORT_URL_PARAMS_NO_WEB = -105;
  // 随机图异常
  Integer RANDOM_IMG_IS_NULL = -201;
  Integer UPLOAD_IMG_IS_NULL = -202;
  Integer UPLOAD_IMG_ONLY_PIC = -203;
  // 获取疫情异常
  Integer EPIDEMIC_IS_NOT_FOUND = -301;
  // 用户异常
  Integer USER_NAME_IS_NOT_FOUND = -401;
  Integer FORUM_TABLE_IS_EMPTY = -402;
  Integer USER_RE_LOGIN = -403;
  Integer EMPTY_SIGN_LIST = -403;
  Integer NOT_SIGN_RECORD = -404;
  Integer NOT_FOUND_USER = -405;
  // user api settings 异常
  Integer USER_TOKEN_VALID = -501;
  Integer USER_REFER_VALID = -502;
  // icon 异常
  Integer GET_ICON_FAIL = -601;
  // 权限异常
  Integer SETTINGS_ID_IS_NOT_FOUND = -901;
  Integer SETTINGS_IS_NO_JSON = -902;
  Integer SETTINGS_IS_EXISTED = -903;
}
