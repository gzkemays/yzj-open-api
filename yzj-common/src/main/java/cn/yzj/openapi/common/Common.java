package cn.yzj.openapi.common;

/**
 * 公用参数
 *
 * @author gzkemays
 * @since 2021/11/11 16:54
 */
public interface Common {
  /** 公用加密盐值 */
  String SECRET_SALT = "YZJ_GROUP_SALT-6320";
  /** 公用空字符串 */
  String EMPTY = "";
  /** 短链接提示词 */
  String SHORT_TIPS = "/s/";
  /** 短链接通用前缀 */
  String SHORT_URL_PREFIX = "http://open.yeziji.xyz/s/";
  /** 时间戳关键字 */
  String TIMESTAMP_KEYWORD = "${TIMESTAMP}";
  /** 页面关键字 */
  String PAGE_KEYWORD = "${PAGE}";
  /** 最高权限密钥 */
  String BEST_AUTH_POWER = "B4TC4CfB6EzApaywh6WXE";
}
