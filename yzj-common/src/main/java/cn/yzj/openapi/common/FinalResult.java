package cn.yzj.openapi.common;

import cn.yzj.openapi.enums.error.ResultEnums;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 最终返回结果
 *
 * @author gzkemays
 * @since 2021/11/11 16:37
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class FinalResult {
  Integer code;
  String msg;
  Object data;

  public static FinalResult buildByResultEnums(ResultEnums enums) {
    return FinalResult.builder().code(enums.getCode()).msg(enums.getMsg()).data(null).build();
  }
}
