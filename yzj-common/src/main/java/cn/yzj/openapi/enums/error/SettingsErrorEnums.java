package cn.yzj.openapi.enums.error;

import cn.yzj.openapi.common.ResultCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 配置异常枚举类
 *
 * @author gzkemays
 * @since 2022/3/30 18:44
 */
@Getter
@AllArgsConstructor
public enum SettingsErrorEnums {
  /** 配置返回异常 */
  SETTINGS_ID_IS_NOT_FOUND(ResultCode.SETTINGS_ID_IS_NOT_FOUND, "找不到指定id"),
  SETTINGS_IS_EXISTED(ResultCode.SETTINGS_IS_EXISTED, "配置已存在"),
  SETTINGS_IS_NO_JSON(ResultCode.SETTINGS_IS_NO_JSON, "配置参数不是为JSON");
  Integer code;
  String msg;
}
