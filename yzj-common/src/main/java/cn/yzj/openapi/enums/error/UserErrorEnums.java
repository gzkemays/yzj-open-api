package cn.yzj.openapi.enums.error;

import cn.yzj.openapi.common.ResultCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 用户错误枚举
 *
 * @author gzkemays
 * @since 2022/6/29 9:35
 */
@Getter
@AllArgsConstructor
public enum UserErrorEnums {
  /** 用户枚举 */
  USER_TOKEN_VALID(ResultCode.USER_TOKEN_VALID, "用户 token 无效"),
  USER_REFER_VALID(ResultCode.USER_REFER_VALID, "用户来源不明");
  Integer code;
  String msg;
}
