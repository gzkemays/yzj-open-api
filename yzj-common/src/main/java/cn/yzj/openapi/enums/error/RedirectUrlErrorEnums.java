package cn.yzj.openapi.enums.error;

import cn.yzj.openapi.common.ResultCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 重定向地址错误枚举类
 *
 * @author gzkemays
 * @since 2022/3/23 17:14
 */
@Getter
@AllArgsConstructor
public enum RedirectUrlErrorEnums {
  /** 重定向地址返回异常 */
  GET_REDIRECT_URL_FAIL(ResultCode.REDIRECT_URL_FAIL, "截取失败");
  Integer code;
  String msg;
}
