package cn.yzj.openapi.enums.error;

import cn.yzj.openapi.common.ResultCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 返回结果枚举类
 *
 * @author gzkemays
 * @since 2021/10/15 20:15
 */
@Getter
@AllArgsConstructor
public enum ResultEnums {
  /** 当请求成功时 */
  OK(ResultCode.SUCCESS, "请求成功"),
  /** 当请求失败时 */
  FAIL(ResultCode.REQUEST_FAIL, "请求失败"),
  HTTP_REQ_NOT_SUPPORTED(ResultCode.METHOD_ERROR, "请求类型错误"),
  DATA_ERROR(ResultCode.DATA_ERROR, "请求数据异常"),
  REQ_ERROR_SUPPORT(ResultCode.HTTP_ERROR, "HTTP请求异常"),
  SQL_ERROR(ResultCode.SQL_ERROR, "数据库异常"),
  SQL_OPERA_ERROR(ResultCode.SQL_OPERA_ERROR, "数据库操作异常"),
  UNKNOWN_ERROR(ResultCode.UNKNOWN_ERROR, "未知异常"),
  NULL_POINTER_ERROR(ResultCode.NULL_POINT_ERROR, "空指针异常"),
  ILLEGAL_ARGS_ERROR(ResultCode.ARGS_ERROR, "参数异常"),
  SECURITY_PWD_ERROR(ResultCode.SECURITY_ERROR, "security加密异常");
  int code;
  String msg;
}
