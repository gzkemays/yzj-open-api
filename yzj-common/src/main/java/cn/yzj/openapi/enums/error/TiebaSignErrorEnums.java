package cn.yzj.openapi.enums.error;

import cn.yzj.openapi.common.ResultCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 贴吧签到异常枚举
 *
 * @author gzkemays
 * @since 2022/4/28 20:44
 */
@Getter
@AllArgsConstructor
public enum TiebaSignErrorEnums {
  /** 贴吧签到异常枚举 */
  USER_NAME_IS_NOT_FOUND(ResultCode.USER_NAME_IS_NOT_FOUND, "无法查询用户名，请确保 Cookie 正确后重试。"),
  NOT_FOUND_USER(ResultCode.NOT_FOUND_USER,"没有该用户"),
  USER_RE_LOGIN(ResultCode.USER_RE_LOGIN, "该用户已经注册过了，如有问题请联系开发者。"),
  EMPTY_SIGN_LIST(ResultCode.EMPTY_SIGN_LIST, "没有需要签到的贴吧"),
  NOT_SIGN_RECORD(ResultCode.NOT_SIGN_RECORD,"尚没有今天的签到记录"),
  FORUM_TABLE_IS_EMPTY(ResultCode.FORUM_TABLE_IS_EMPTY, "获取关注列表失败");
  Integer code;
  String msg;
}
