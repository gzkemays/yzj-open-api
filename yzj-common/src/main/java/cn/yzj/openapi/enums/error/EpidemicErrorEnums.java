package cn.yzj.openapi.enums.error;

import cn.yzj.openapi.common.ResultCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author gzkemays
 * @since 2022/4/14 11:40
 */
@Getter
@AllArgsConstructor
public enum EpidemicErrorEnums {
  /** 随机图返回异常 */
  EPIDEMIC_IS_NOT_FOUND(ResultCode.EPIDEMIC_IS_NOT_FOUND, "没有相关地区的疫情情况");
  Integer code;
  String msg;
}
