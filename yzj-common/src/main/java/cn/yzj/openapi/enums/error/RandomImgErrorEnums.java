package cn.yzj.openapi.enums.error;

import cn.yzj.openapi.common.ResultCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 随机图异常枚举类
 *
 * @author gzkemays
 * @since 2022/3/30 18:13
 */
@Getter
@AllArgsConstructor
public enum RandomImgErrorEnums {
  /** 随机图返回异常 */
  RANDOM_IMG_IS_NULL(ResultCode.RANDOM_IMG_IS_NULL, "随机图为空"),
  UPLOAD_IMG_IS_NULL(ResultCode.UPLOAD_IMG_IS_NULL, "上传文件不能为空"),
  UPLOAD_IMG_ONLY_PIC(ResultCode.UPLOAD_IMG_ONLY_PIC, "上传的文件仅允许是图片");
  Integer code;
  String msg;
}
