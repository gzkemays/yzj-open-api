package cn.yzj.openapi.enums.error;

import cn.yzj.openapi.common.ResultCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * icon 错误枚举
 *
 * @author gzkemays
 * @since 2022/6/29 22:39
 */
@Getter
@AllArgsConstructor
public enum IconErrorEnums {
  /** icon 错误枚举 */
  GET_ICON_FAIL(ResultCode.GET_ICON_FAIL, "获取 icon 失败");
  Integer code;
  String msg;
}
