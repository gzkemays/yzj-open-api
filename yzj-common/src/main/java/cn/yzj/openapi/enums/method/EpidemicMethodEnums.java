package cn.yzj.openapi.enums.method;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 疫情方法枚举
 *
 * @author gzkemays
 * @since 2022/4/14 12:06
 */
@Getter
@AllArgsConstructor
public enum EpidemicMethodEnums {
  /** 疫情查询方法 */
  DOMESTIC("国内", "caseList"),
  CHINA("中国", "summaryDataIn"),
  OUTSIDE("国外", "caseOutsideList");
  String position;
  String modeKey;
}
