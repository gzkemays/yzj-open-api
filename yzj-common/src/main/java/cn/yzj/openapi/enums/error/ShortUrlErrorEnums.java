package cn.yzj.openapi.enums.error;

import cn.yzj.openapi.common.ResultCode;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 短链接错误枚举类
 *
 * @author gzkemays
 * @since 2021/11/12 0:11
 */
@Getter
@AllArgsConstructor
public enum ShortUrlErrorEnums {
  /** 短链接信息返回 */
  EMPTY_ORIGIN(ResultCode.SHORT_URL_EMPTY_ERROR, "短地址为空"),
  OVER_TIME(ResultCode.SHORT_URL_TIME_OVER_ERROR, "链接已超时"),
  NO_WEB(ResultCode.SHORT_URL_PARAMS_NO_WEB, "链接不是为网址"),
  CLOSED_URL(ResultCode.SHORT_URL_CLOSED_ERROR, "链接已关闭"),
  CHECK_FAIL(ResultCode.SHORT_URL_CHECK_ERROR, "校验失败");
  Integer code;
  String msg;
}
