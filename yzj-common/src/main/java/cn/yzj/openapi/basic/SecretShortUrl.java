package cn.yzj.openapi.basic;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * url 加密对象
 * @author gzkemays
 * @since 2021/11/11 23:15
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class SecretShortUrl {
  /** 盐值 */
  String salt;
  /** 密钥 */
  String key;
  /** 源地址 */
  String originUrl;
  /** 短链接 */
  String shortUrl;
}
