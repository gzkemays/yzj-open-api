package cn.yzj.openapi.basic;

import cn.yzj.entity.AliOssConfig;
import cn.yzj.openapi.common.YamlAndPropertySourceFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import java.io.InputStream;

/**
 * 阿里云 OSS 基本配置
 *
 * @author gzkemays
 * @since 2022/4/13 10:53
 */
@Component
@PropertySource(
    value = "classpath:aliyun.yml",
    name = "aliyun.yml",
    factory = YamlAndPropertySourceFactory.class)
@ConfigurationProperties(prefix = "oss")
@Data
public class AliCloudOssBase {
  String accessKey;
  String securityKey;
  String bucket;
  String endPoint;
  String savePath;
  String returnUrl;

  public AliOssConfig buildConfig(String filename, String ext, InputStream fileStream) {
    return AliOssConfig.builder()
        .accessKey(accessKey)
        .secretKey(securityKey)
        .bucketName(bucket)
        .endPoint(endPoint)
        .savePath(savePath)
        .fileName(filename)
        .ext(ext)
        .stream(fileStream)
        .build();
  }
}
