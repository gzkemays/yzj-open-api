package cn.yzj.openapi.basic;

import cn.yzj.openapi.common.YamlAndPropertySourceFactory;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 签到 cookie
 *
 * @author gzkemays
 * @since 2022/5/1 20:26
 */
@Component
@PropertySource(
    value = "classpath:sign.yml",
    name = "sign.yml",
    factory = YamlAndPropertySourceFactory.class)
@ConfigurationProperties(prefix = "cookie")
@Data
public class SignCookie {
  String cunhua;
  WebBasicAuthRequest xiaoaishe;
}
