package cn.yzj.openapi.basic;

import lombok.Data;

/**
 * @author gzkemays
 * @date 2022/5/9
 **/
@Data
public class WebBasicAuthRequest {
    String cookie;
    String token;
}
