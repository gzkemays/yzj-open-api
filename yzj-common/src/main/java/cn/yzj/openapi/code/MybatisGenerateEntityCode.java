package cn.yzj.openapi.code;

import cn.org.atool.generator.FileGenerator;
import cn.org.atool.generator.annotation.Table;
import cn.org.atool.generator.annotation.Tables;

/**
 * Mybatis 实体类生成类
 *
 * @author gzkemays
 * @since 2021/11/11 16:06
 */
public class MybatisGenerateEntityCode {
  public static final String URL =
      "jdbc:mysql://127.0.0.1:4096/yzj_open_api?useSSL=false&serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=UTF-8&allowPublicKeyRetrieval=true";
  public static final String USER_NAME = "root";
  public static final String PASSWORD = "gzkemays";
  public static final String SRC_DIR = "yzj-dao\\src\\main\\java";
  public static final String BASE_PACK = "cn.yzj.openapi";
  public static final String DAO_DIR = "yzj-dao\\src\\main\\java";

  public static void main(String[] args) {
    FileGenerator.build(Entity.class);
  }

  @Tables(
      url = URL,
      username = USER_NAME,
      password = PASSWORD,
      srcDir = SRC_DIR,
      basePack = BASE_PACK,
      daoDir = DAO_DIR,
      tables = {@Table(value = {"api_settings"})})
  static class Entity {}
}
