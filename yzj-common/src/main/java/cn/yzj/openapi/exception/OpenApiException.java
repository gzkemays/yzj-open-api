package cn.yzj.openapi.exception;

import cn.yzj.openapi.enums.error.*;
import lombok.*;

/**
 * 短链接异常
 *
 * @author gzkemays
 * @since 2021/11/12 0:00
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class OpenApiException extends RuntimeException {
  Integer code;
  String msg;

  public static OpenApiException shortUrlException(ShortUrlErrorEnums enums) {
    return OpenApiException.builder().code(enums.getCode()).msg(enums.getMsg()).build();
  }

  public static OpenApiException randomImgException(RandomImgErrorEnums enums) {
    return OpenApiException.builder().code(enums.getCode()).msg(enums.getMsg()).build();
  }

  public static OpenApiException settingsException(SettingsErrorEnums enums) {
    return OpenApiException.builder().code(enums.getCode()).msg(enums.getMsg()).build();
  }

  public static OpenApiException tiebaSignException(TiebaSignErrorEnums enums) {
    return OpenApiException.builder().code(enums.getCode()).msg(enums.getMsg()).build();
  }

  public static OpenApiException epidemicException(EpidemicErrorEnums enums) {
    return OpenApiException.builder().code(enums.getCode()).msg(enums.getMsg()).build();
  }

  public static OpenApiException redirectUrlException(RedirectUrlErrorEnums enums) {
    return OpenApiException.builder().code(enums.getCode()).msg(enums.getMsg()).build();
  }

  public static OpenApiException userException(UserErrorEnums enums) {
    return OpenApiException.builder().code(enums.getCode()).msg(enums.getMsg()).build();
  }

  public static OpenApiException iconException(IconErrorEnums enums) {
    return OpenApiException.builder().code(enums.getCode()).msg(enums.getMsg()).build();
  }
}
