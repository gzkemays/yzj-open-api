package cn.yzj.openapi.bo;

import lombok.Data;

import java.util.List;

/**
 * 全球疫情情况
 *
 * @author gzkemays
 * @since 2022/4/14 12:47
 */
@Data
public class EpidemicGlobalBO {
  String area;
  List<EpidemicGlobalBO> subList;
  String curedPercent;
  String confirmedRelative;
  String diedPercent;
  String died;
  String curConfirm;
  String crued;
  String confirmed;
}
