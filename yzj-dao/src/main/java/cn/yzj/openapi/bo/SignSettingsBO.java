package cn.yzj.openapi.bo;

import cn.yzj.openapi.entity.ApiSettingsEntity;
import com.alibaba.fastjson.JSONObject;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 签到配置BO
 *
 * @author gzkemays
 * @since 10/16/2022
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SignSettingsBO {
  boolean sign;
  String baseUrl;
  String username;
  String password;

  public SignSettingsBO(boolean sign, String baseUrl) {
    this.sign = sign;
    this.baseUrl = baseUrl;
  }

  public static List<SignSettingsBO> buildList(ApiSettingsEntity entity) {
    List<SignSettingsBO> list = new ArrayList<>();
    JSONObject json = JSONObject.parseObject(entity.getSettings());
    for (Map.Entry<String, Object> entry : json.entrySet()) {
      JSONObject settingMsg = (JSONObject) entry.getValue();
      list.add(
          new SignSettingsBO(
              (Boolean) settingMsg.get("sign"),
              (String) settingMsg.get("baseUrl"),
              (String) settingMsg.get("username"),
              (String) settingMsg.get("password")));
    }
    return list;
  }
}
