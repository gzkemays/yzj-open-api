package cn.yzj.openapi.bo;

import lombok.Data;

import java.util.List;

/**
 * 地区疫情BO
 *
 * @author gzkemays
 * @since 2022/4/14 10;50
 */
@Data
public class EpidemicAreaBO {
  /** 地区 */
  String area;
  /** 境外输入 */
  String overseasInputRelative;
  /** 城市列表 */
  List<EpidemicCityBO> subList;
  /** 死亡数 */
  String diedRelative;
  /** 新增本土 */
  String nativeRelative;
  /** 无症状 */
  String asymptomatic;
  /** 城市编码 */
  String cityCode;
  /** 筛查出为阳性 */
  String screeningPositive;
  /** 更新时间 */
  String updateTime;
  /** 累计死亡 */
  String died;
  /** 新增无症状 */
  String asymptomaticRelative;

  String curConfirmRelative;
  /** 累计确诊 */
  String confirmed;

  /**
   * 累计治愈
   */
  String crued;
  String relativeTime;
  String noNativeRelativeDays;
  /**
   * 新增
   */
  String confirmedRelative;
  String icuDisable;
  String curedRelative;
  String curConfirm;
}
