package cn.yzj.openapi.bo;

import lombok.Data;

/**
 * 疫情数据对象
 *
 * @author gzkemays
 * @since 2022/4/14 10:38
 */
@Data
public class EpidemicCityBO {
  /** 城市 */
  String city;
  /** 新增本土 */
  String nativeRelative;
  /** 无症状 */
  String asymptomatic;
  /** 城市编码 */
  String cityCode;
  /** 筛查出为阳性 */
  String screeningPositive;
  /** 更新时间 */
  String updateTime;

  String relativeTime;
  /** 累计死亡 */
  String died;
  /** 新增无症状 */
  String asymptomaticRelative;
  /** 累计确诊 */
  String confirmed;
  /** 累计治愈 */
  String crued;

  /** 无新增描述 */
  String noNativeRelativeDays;
  /** 确认本土 */
  String confirmedRelative;
  /** 现有确诊 */
  String curConfirm;
}
