package cn.yzj.openapi.bo;

import lombok.Data;

/**
 * 国家疫情
 *
 * @author gzkemays
 * @since 2022/4/14 12:48
 */
@Data
public class EpidemicCountryBO {
  String relativeTime;
  String country;
  String confirmedRelative;
  String died;
  String curConfirm;
  String confirmed;
  String crued;
}
