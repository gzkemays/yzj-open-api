package cn.yzj.openapi.converter;

import cn.yzj.openapi.bo.EpidemicAreaBO;
import cn.yzj.openapi.bo.EpidemicCityBO;
import cn.yzj.openapi.vo.EpidemicVO;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

/**
 * 疫情对象转换器
 *
 * @author gzkemays
 * @since 2022/4/14 11:34
 */
@Mapper
public interface EpidemicConverter {
  EpidemicConverter INSTANCE = Mappers.getMapper(EpidemicConverter.class);

  @Mapping(target = "position", source = "area")
  EpidemicVO areaBoConverterVo(EpidemicAreaBO bo);

  List<EpidemicVO> areaBosConverterVos(List<EpidemicAreaBO> bo);

  @Mapping(target = "position", source = "city")
  EpidemicVO cityBoConverterVo(EpidemicCityBO bo);

  List<EpidemicVO> cityBosConverterVos(List<EpidemicCityBO> bo);
}
