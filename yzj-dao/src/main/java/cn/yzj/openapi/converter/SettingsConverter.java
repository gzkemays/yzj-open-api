package cn.yzj.openapi.converter;

import cn.yzj.openapi.dto.SettingsDTO;
import cn.yzj.openapi.entity.ApiSettingsEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * @author gzkemays
 * @since 2022/3/30 18:38
 */
@Mapper
public interface SettingsConverter {
  SettingsConverter INSTANCE = Mappers.getMapper(SettingsConverter.class);

  ApiSettingsEntity convertEntityByDTO(SettingsDTO dto);
}
