package cn.yzj.openapi.converter;

import cn.yzj.openapi.basic.SecretShortUrl;
import cn.yzj.openapi.dto.UrlDTO;
import cn.yzj.openapi.entity.ShortUrlEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

/**
 * 短链接数据转换器
 *
 * @author gzkemays
 * @since 2021/11/11 16:44
 */
@Mapper
public interface ShortUrlConverter {
  ShortUrlConverter INSTANCE = Mappers.getMapper(ShortUrlConverter.class);

  /**
   * 根据 dto 转化为 entity
   *
   * @param dto dto 值入参对象
   * @return entity 对象
   */
  @Mapping(target = "originUrl", source = "url")
  ShortUrlEntity convertEntityByDTO(UrlDTO dto);

  /**
   * 根据生成的加密短链接转化为 entity 所需参数
   *
   * @param bo 短链接数据对象
   * @return entity 对象
   */
  ShortUrlEntity convertEntityBySecretShortUrlBo(SecretShortUrl bo);
}
