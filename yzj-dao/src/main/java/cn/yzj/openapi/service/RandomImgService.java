package cn.yzj.openapi.service;

import org.springframework.web.multipart.MultipartFile;

/**
 * 随机图服务
 *
 * @author gzkemays
 * @since 2022/3/24 19:29
 */
public interface RandomImgService {

  String OSS_IMG_API = "https://img.yeziji.xyz/QQImage/OSS/${IMG}";
  String IMG_KEY = "${IMG}";
  Integer OPEN_THREAD_COUNT = 5;

  /**
   * 获取随机图
   *
   * @return 图片地址
   */
  String getRandomImg();

  /**
   * 上传文件
   *
   * @param files 文件数组
   * @return 上传结果
   */
  String upload(MultipartFile[] files);
}
