package cn.yzj.openapi.service;

import cn.yzj.openapi.basic.WebBasicAuthRequest;
import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 签到服务
 *
 * @author gzkemays
 * @since 2022/5/1 20:27
 */
public interface SignService {
  String PREFIX = "${PREFIX}";
  String FULI_USER_MISSION_URL = "https://${PREFIX}/wp-json/b2/v1/userMission";
  String FULI_TEMP_TOKEN_URL = "https://${PREFIX}/wp-json/b2/v1/getRecaptcha";
  String FULI_TOKEN_URL = "https://${PREFIX}/wp-json/jwt-auth/v1/token";
  String FULI_GET_USER_MISSION_URL = "https://${PREFIX}/wp-json/b2/v1/getUserMission";
  String FULI_NEW_MSG_URL = "https://${PREFIX}/wp-json/b2/v1/getNewDmsg";
  String FULI_REFRESH_MSG = "https://${PREFIX}/wp-json/b2/v1/getTaskData";

  void tiebaSign();

  void fuliSign();

  void sfSign();

  default Map<String, String> getHeaders(WebBasicAuthRequest request, FuliBasicUrls urls) {
    Map<String, String> headers = new HashMap<>();
    //    headers.put("Cookie", request.getCookie());
    headers.put("Authorization", request.getToken());
    headers.put("Host", urls.getBaseUrl());
    headers.put("Referer", urls.getReferer());
    headers.put("Origin", urls.getOrigin());
    return headers;
  }

  @Data
  class FuliBasicUrls {
    String baseUrl;
    String referer;
    String origin;
    String userMissionUrl;
    String tempTokenUrl;
    String tokenUrl;
    String userMissionUrlGet;
    String newUserMsgUrl;
    String refreshUrl;

    public FuliBasicUrls(String baseUrl) {
      this.baseUrl = baseUrl;
      this.setReferer("https://${PREFIX}.com/task".replace(PREFIX, baseUrl));
      this.setOrigin("https://${PREFIX}".replace(PREFIX, baseUrl));
      this.setUserMissionUrl(FULI_USER_MISSION_URL.replace(PREFIX, baseUrl));
      this.setTempTokenUrl(FULI_TEMP_TOKEN_URL.replace(PREFIX, baseUrl));
      this.setTokenUrl(FULI_TOKEN_URL.replace(PREFIX, baseUrl));
      this.setUserMissionUrlGet(FULI_GET_USER_MISSION_URL.replace(PREFIX, baseUrl));
      this.setNewUserMsgUrl(FULI_NEW_MSG_URL.replace(PREFIX, baseUrl));
      this.setRefreshUrl(FULI_REFRESH_MSG.replace(PREFIX, baseUrl));
    }
  }
}
