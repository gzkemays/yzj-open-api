package cn.yzj.openapi.service;

import cn.yzj.openapi.dto.EmailDTO;

/**
 * Email 发送服务
 *
 * @author gzkemays
 * @since 2022/4/15 17:34
 */
public interface EmailService {
  /**
   * 发送验证码
   *
   * @param to 发送目标
   * @return 返回验证码
   */
  String sendVerificationCode(EmailDTO dto);

  void send(EmailDTO dto);

}
