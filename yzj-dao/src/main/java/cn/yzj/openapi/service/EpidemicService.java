package cn.yzj.openapi.service;

import cn.yzj.openapi.dto.EpidemicQueryDTO;
import cn.yzj.openapi.vo.EpidemicVO;

import java.util.ArrayList;
import java.util.List;

/**
 * 疫情查询接口
 *
 * @author gzkemays
 * @since 2022/4/14 11:12
 */
public interface EpidemicService {
  String BAIDU_EPIDEMIC_SOURCE = "https://voice.baidu.com/act/newpneumonia/newpneumonia";
  String EPIDEMIC_ALL_DATA = "allEpidemic";
  List<String> CHINA_VOCABULARY =
      new ArrayList<String>() {
        {
          add("我的国");
          add("我滴国");
          add("中国");
          add("我国");
          add("本国");
          add("中华");
        }
      };

  String CHINA = "中国";

  /**
   * 获取地区疫情
   *
   * @param dto 疫情情况
   * @return 地区疫情情况
   */
  List<EpidemicVO> getEpidemic(EpidemicQueryDTO dto);

  /**
   * 模糊查询获取地区疫情
   *
   * @param position 地区
   * @return 疫情情况
   */
  List<EpidemicVO> getEpidemic(String position);


}
