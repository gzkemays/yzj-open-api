package cn.yzj.openapi.service;

/**
 * 用户服务
 *
 * @author gzkemays
 * @since 2022/6/24 9:16
 */
public interface UserService {
    void login();
}
