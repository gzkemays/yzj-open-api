package cn.yzj.openapi.service;

/**
 * Url 服务
 *
 * @author gzkemays
 * @since 2022/3/23 17:06
 */
public interface UrlService {
  /**
   * 获取重定向地址
   *
   * @param url 原地址
   * @return 重定向地址
   */
  String getRedirectUrl(String url);
}
