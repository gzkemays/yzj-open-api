package cn.yzj.openapi.service.impl;

import cn.yzj.common.HttpMethod;
import cn.yzj.http.YzjHttp;
import cn.yzj.http.YzjHttpResult;
import cn.yzj.http.YzjSendConfig;
import cn.yzj.openapi.enums.error.RedirectUrlErrorEnums;
import cn.yzj.openapi.exception.OpenApiException;
import cn.yzj.openapi.service.UrlService;
import org.springframework.stereotype.Repository;

/**
 * url 服务实现类
 *
 * @author gzkemays
 * @since 2022/3/23 17:06
 */
@Repository
public class UrlServiceImpl implements UrlService {
  @Override
  public String getRedirectUrl(String url) {
    YzjHttpResult send =
        YzjHttp.create()
            .method(HttpMethod.GET)
            .url(url)
            .config(YzjSendConfig.start().redirect().document().end())
            .send();
    String redirectUrl = send.getRedirectUrl();
    if (redirectUrl != null && !redirectUrl.equals(url)) {
      return redirectUrl;
    }
    throw OpenApiException.redirectUrlException(RedirectUrlErrorEnums.GET_REDIRECT_URL_FAIL);
  }
}
