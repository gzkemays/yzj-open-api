package cn.yzj.openapi.service;

/**
 * 微信服务
 *
 * @author gzkemays
 * @since 2021/12/14 15:35
 */
public interface WxService {

  /**
   * 获取微信二维码
   *
   * @param basic 生成基本数据要求
   * @return 返回 buffer
   */
}
