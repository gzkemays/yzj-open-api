package cn.yzj.openapi.service.impl;

import cn.yzj.openapi.service.MagneticLinkService;
import org.springframework.stereotype.Repository;

/**
 * 磁链接服务实现类
 * @author gzkemays
 * @since 2022/3/24 17:12
 */
@Repository
public class MagneticLinkServiceImpl implements MagneticLinkService {
}
