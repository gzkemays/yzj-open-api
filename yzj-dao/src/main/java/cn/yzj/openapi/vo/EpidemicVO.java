package cn.yzj.openapi.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 疫情返回对象
 *
 * @author gzkemays
 * @since 2022/4/14 11:13
 */
@Data
@JsonInclude(JsonInclude.Include.NON_NULL)
public class EpidemicVO {
  /** 位置 */
  String position;
  /** 新增本土 */
  String confirmedRelative;
  /** 累计确诊 */
  String confirmed;
  /** 累计治愈 */
  String crued;
  /** 累计死亡 */
  String died;
  /** 现有确诊 */
  String curConfirm;
  /** 无症状 */
  String asymptomaticRelative;
  /** 更新时间 */
  String updateTime;

  String relativeTime;

  String desc =
      "position：地区，confirmedRelative：新增本土，confirmed：累计确诊，crued：累计治愈，died：累计死亡，curConfirm：现有确诊，asymptomaticRelative：新增无症状，updateTime：数据更新时间； —— 数据来源于百度疫情实时大数据";
  static SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

  public void setUpdateTime(String updateTime) {
    if (StringUtils.isNotBlank(updateTime)) {
      long timestamp = Long.parseLong(updateTime);
      this.updateTime = SDF.format(new Date(timestamp * 1000));
    } else {
      this.updateTime = updateTime;
    }
  }

  public void setRelativeTime(String relativeTime) {
    if (StringUtils.isBlank(updateTime)) {
      if (StringUtils.isNotBlank(relativeTime)) {
        long timestamp = Long.parseLong(relativeTime);
        this.updateTime = SDF.format(new Date(timestamp * 1000));
      } else {
        this.updateTime = relativeTime;
      }
    }
  }

  @Override
  public String toString() {
    return "疫情反馈{"
        + "地区='"
        + position
        + '\''
        + "新增本土='"
        + confirmedRelative
        + '\''
        + ", 累计确诊='"
        + confirmed
        + '\''
        + ", 累计治愈='"
        + crued
        + '\''
        + ", 累计死亡='"
        + died
        + '\''
        + ", 现有确诊='"
        + curConfirm
        + '\''
        + ", 获取时间='"
        + updateTime
        + '\''
        + '}';
  }
}
