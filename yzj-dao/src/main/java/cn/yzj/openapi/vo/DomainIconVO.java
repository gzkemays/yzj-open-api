package cn.yzj.openapi.vo;

import cn.yzj.openapi.entity.DomainIconEntity;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 网址图标 VO 对象
 *
 * @author gzkemays
 * @since 2022/6/19 17:55
 */
@Data
public class DomainIconVO {
  String domain;
  List<String> urls;

  public DomainIconVO(DomainIconEntity entity) {
    String icon = entity.getIcon();
    if (StringUtils.isNotBlank(icon)) {
      this.urls = Arrays.asList(icon.split(","));
    }
    this.domain = entity.getDomain();
  }

  public static List<DomainIconVO> listEntity(List<DomainIconEntity> entities) {
    List<DomainIconVO> list = new ArrayList<>();
    for (DomainIconEntity entity : entities) {
      list.add(new DomainIconVO(entity));
    }
    return list;
  }
}
