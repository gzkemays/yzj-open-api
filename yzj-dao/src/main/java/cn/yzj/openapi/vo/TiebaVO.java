package cn.yzj.openapi.vo;

import cn.yzj.openapi.utils.TiebaUtils;
import lombok.Data;

import java.util.List;

/**
 * @author gzkemays
 * @since 2022/4/28 22:03
 */
@Data
public class TiebaVO {
  String username;
  List<String> follows;
  Integer count;

  public TiebaVO(TiebaUtils utils) {
    this.username = utils.getUsername();
    this.follows = utils.getFollows();
    this.count = follows.size();
  }
}
