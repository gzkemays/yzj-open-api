package cn.yzj.openapi.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 网址图标 DTO
 *
 * @author gzkemays
 * @since 2022/6/18 12:40
 */
@Data
@NoArgsConstructor
public class DomainIconDTO {
  String domain;
  String alias;
  String icon;

  public DomainIconDTO(String domain) {
    this.domain = domain;
  }
}
