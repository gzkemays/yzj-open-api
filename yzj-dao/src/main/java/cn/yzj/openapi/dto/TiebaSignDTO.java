package cn.yzj.openapi.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 贴吧登录
 * @author gzkemays
 * @since 2022/4/28 23:26
 */
@Data
public class TiebaSignDTO {
    @NotBlank(message = "cookie 不能为空")
    String cookie;
    Boolean relogin;
}
