package cn.yzj.openapi.dto;

import lombok.Data;

/**
 * 用户登录值对象
 *
 * @author gzkemays
 * @since 2022/6/24 9:18
 */
@Data
public class UserLoginDTO {
  String username;
  String password;
}
