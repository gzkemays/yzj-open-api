package cn.yzj.openapi.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 配置文件值对象
 *
 * @author gzkemays
 * @since 2022/3/30 18:34
 */
@Data
public class SettingsDTO {
  @NotBlank(message = "id 不能为空")
  String id;

  String settings;
}
