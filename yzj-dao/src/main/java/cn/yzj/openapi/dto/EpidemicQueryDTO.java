package cn.yzj.openapi.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * 疫情查询dto
 *
 * @author gzkemays
 * @since 2022/4/14 11:13
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EpidemicQueryDTO {
  @NotBlank(message = "至少需要声明地区")
  String area;

  String city;
}
