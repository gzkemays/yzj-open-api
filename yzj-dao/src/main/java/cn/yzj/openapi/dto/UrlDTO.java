package cn.yzj.openapi.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 短链接 dto
 *
 * @author gzkemays
 * @since 2021/11/11 16:39
 */
@Data
public class UrlDTO {
  @NotBlank(message = "地址 url 不能为空")
  String url;
}
