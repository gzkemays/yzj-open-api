package cn.yzj.openapi.dto;

import lombok.Data;
/**
 * 随机图登录值对象
 *
 * @author gzkemays
 * @since 2022/3/24 16:57
 */
@Data
public class RandomImgLoginDTO {
  String formhash;
  String username;
  String password;
  Integer questionid;
  String answer;
  Integer cookietime;
  String referer;

  public RandomImgLoginDTO(String formhash) {
    this.formhash = formhash;
    answer = "PC";
    referer = "https://cosaac.cyou/./";
    cookietime = 2592000;
    questionid = 5;
    password = "a96548854";
    username = "gzkemays";
  }
}
