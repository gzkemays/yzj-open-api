package cn.yzj.openapi.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

/**
 * @author gzkemays
 * @since 2022/4/15 17:58
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class EmailDTO {
  @NotBlank(message = "发送目标不能为空")
  String to;

  @NotBlank(message = "标题不能为空")
  String subject;

  String additional;
  String code;
  String message;
}
