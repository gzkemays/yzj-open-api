package cn.yzj.openapi.dto;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 生成微信二维码
 *
 * @author gzkemays
 * @since 2021/12/14 15:07
 */
@Data
public class WxQrCodeDTO {
  @NotBlank(message = "appId 不能为空")
  String appid;

  @NotBlank(message = "secret 不能为空")
  String secret;
}
