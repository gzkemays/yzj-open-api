package cn.yzj.openapi.dao.impl;

import cn.yzj.http.YzjHttp;
import cn.yzj.http.YzjHttpResult;
import cn.yzj.http.YzjSendConfig;
import cn.yzj.openapi.dao.base.DomainIconBaseDao;
import cn.yzj.openapi.dao.intf.DomainIconDao;
import cn.yzj.openapi.dto.DomainIconDTO;
import cn.yzj.openapi.entity.DomainIconEntity;
import cn.yzj.openapi.enums.error.IconErrorEnums;
import cn.yzj.openapi.exception.OpenApiException;
import cn.yzj.openapi.vo.DomainIconVO;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.util.CollectionUtils;

import java.util.Date;
import java.util.List;
import java.util.StringJoiner;

/**
 * DomainIconDaoImpl: 数据操作接口实现
 *
 * <p>这只是一个减少手工创建的模板文件 可以任意添加方法和实现, 更改作者和重定义类名
 *
 * <p>@author Powered By Fluent Mybatis
 */
@Repository
public class DomainIconDaoImpl extends DomainIconBaseDao implements DomainIconDao {
  @Override
  public DomainIconEntity getIconByDomain(DomainIconDTO dto) {
    StringJoiner sj = new StringJoiner(",");
    String customerIcon, domain = null;
    DomainIconEntity entity = null;
    if (StringUtils.isNotBlank(dto.getDomain())
        && (entity = mapper.findOne(query().where.domain().like(dto.getDomain()).end())) == null) {
      // 如果不是用户自定义 icon 则请求查询
      if ((customerIcon = dto.getIcon()) == null) {
        String api = getIconUrl(dto.getDomain());
        YzjHttpResult send =
            YzjHttp.createGet(api)
                .defaultUserAgent()
                .config(YzjSendConfig.start().json().end())
                .send();
        JSONObject json = send.getJson();
        JSONArray icons = json.getJSONArray("icons");
        String tmpicon = null;
        domain = json.getString("domain");
        if (icons == null) {
          List<String> getToHttpIcon = createGetToHttpIcon(domain);
          for (String icon : getToHttpIcon) {
            if (tmpicon == null) {
              sj.add((tmpicon = icon));
            } else if (!sj.toString().contains(icon)) {
              sj.add(icon);
            }
          }
        } else {
          for (Object icon : icons) {
            JSONObject iconJson = (JSONObject) icon;
            String iconStr = iconJson.getString("src");
            if (tmpicon == null) {
              sj.add((tmpicon = iconStr));
            } else if (!sj.toString().contains(iconStr)) {
              sj.add(iconStr);
            }
          }
        }
      }
      entity = new DomainIconEntity();
      entity.setDomain(domain);
      entity.setIcon(customerIcon != null ? customerIcon : sj.toString());
      entity.setCreateTime(new Date());
      mapper.insert(entity);
    }
    return entity;
  }

  @Override
  public DomainIconEntity getIconByDomainByHttp(DomainIconDTO dto) {
    String domain;
    DomainIconEntity entity = null;
    if (StringUtils.isNotBlank((domain = dto.getDomain()))
        && (entity = mapper.findOne(query().where.domain().like(domain).end())) == null) {
      StringJoiner iconSj = new StringJoiner(",");
      List<String> icons = createGetToHttpIcon(domain);
      if (!CollectionUtils.isEmpty(icons)) {
        String tmpicon = null;
        for (String icon : icons) {
          if (tmpicon == null) {
            iconSj.add((tmpicon = icon));
          } else if (!iconSj.toString().contains(icon)) {
            iconSj.add(icon);
          }
        }
        entity = new DomainIconEntity();
        entity.setDomain(domain);
        entity.setIcon(iconSj.toString());
        entity.setCreateTime(new Date());
        mapper.insert(entity);
        return entity;
      } else {
        throw OpenApiException.iconException(IconErrorEnums.GET_ICON_FAIL);
      }
    }
    return entity;
  }

  @Override
  public List<DomainIconVO> getDomainIconList(List<String> domains) {
    if (!domains.isEmpty()) {
      return DomainIconVO.listEntity(mapper.listEntity(query().where.domain().in(domains).end()));
    }
    return null;
  }
}
