package cn.yzj.openapi.dao.intf;

import cn.org.atool.fluent.mybatis.base.IBaseDao;
import cn.yzj.openapi.dto.TiebaSignDTO;
import cn.yzj.openapi.entity.TiebaSignEntity;
import cn.yzj.openapi.vo.TiebaVO;

/**
 * 贴吧签到接口
 *
 * @author gzkemays
 * @since 2022/04/28 20:35
 */
public interface TiebaSignDao extends IBaseDao<TiebaSignEntity> {
  /** 打卡操作 */
  void sign();

  void test();

  /**
   * 手动签到
   *
   * @param username 用户名
   * @return 签到结果
   */
  String sign(String username);

  /**
   * 保存用户信息
   *
   * @param dto 保存对象
   * @return 保存时会查询用户贴吧数据并返回
   */
  TiebaVO save(TiebaSignDTO dto);

  /**
   * 查询用户当天的签到情况
   *
   * @param username 用户名
   * @return 签到状态
   */
  String checkSign(String username);
}
