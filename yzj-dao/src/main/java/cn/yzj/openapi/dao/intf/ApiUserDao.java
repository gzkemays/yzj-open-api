package cn.yzj.openapi.dao.intf;

import cn.org.atool.fluent.mybatis.base.IBaseDao;
import cn.yzj.openapi.entity.ApiUserEntity;
import com.alibaba.fastjson.JSONObject;

/**
 * api user 服务
 *
 * @author gzkemays
 * @since 2022/06/29 09:26
 */
public interface ApiUserDao extends IBaseDao<ApiUserEntity> {
  String CLOUD_CONFIG_CACHE_KEY = "OpenApi::User::Config::";
  /**
   * 用户云配置存根
   * @param navigate 配置 json
   * @return 更改结果
   */
  int cloudConfigSave(JSONObject navigate);

  /**
   * 获取云配置
   *
   * @param username 用户名
   * @return 配置的 json
   */
  JSONObject getCloudConfig(String refer, String username);
}
