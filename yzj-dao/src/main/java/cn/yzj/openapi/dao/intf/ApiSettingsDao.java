package cn.yzj.openapi.dao.intf;

import cn.org.atool.fluent.mybatis.base.IBaseDao;
import cn.yzj.openapi.dto.SettingsDTO;
import cn.yzj.openapi.entity.ApiSettingsEntity;

import java.util.HashSet;
import java.util.Set;

/**
 * api 设置表
 *
 * @author gzkemays
 * @since 2022/03/30 17:18
 */
public interface ApiSettingsDao extends IBaseDao<ApiSettingsEntity> {
  String IMG_COUNT_LIST_CACHE_KEY = "IMGCOUNT";
  String PIC_MAX_COUNT = "picMaxCount";

  Integer IMG = 1;
  Integer ALL = 0;

  /**
   * 生成指定 min ~ max 的 set 列表
   *
   * @param min 最小值
   * @param max 最大值
   * @return set
   */
  default Set<Integer> generateNumberSet(int min, int max) {
    if (max <= min) {
      throw new NullPointerException("最大值不能小于或等于最小值");
    }
    Set<Integer> set = new HashSet<>();
    for (int i = min; i <= max; i++) {
      set.add(i);
    }
    return set;
  }
  /**
   * 更新配置
   *
   * @param entity 配置
   */
  void update(ApiSettingsEntity entity);

  void insertSettings(SettingsDTO dto);

  /**
   * 重置配置缓存
   *
   * @param mode 重置类型
   */
  void resetSettingCache(Integer mode);
}
