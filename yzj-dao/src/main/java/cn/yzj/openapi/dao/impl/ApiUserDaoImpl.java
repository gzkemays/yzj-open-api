package cn.yzj.openapi.dao.impl;

import cn.yzj.openapi.dao.base.ApiUserBaseDao;
import cn.yzj.openapi.dao.intf.ApiUserDao;
import cn.yzj.openapi.entity.ApiUserEntity;
import cn.yzj.openapi.enums.error.UserErrorEnums;
import cn.yzj.openapi.exception.OpenApiException;
import cn.yzj.openapi.utils.JedisUtils;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * api user 服务逻辑实现层
 *
 * @author gzkemays
 * @since 2022/06/29 09:26
 */
@Repository
public class ApiUserDaoImpl extends ApiUserBaseDao implements ApiUserDao {
  @Resource JedisUtils jedis;
  @Resource HttpServletRequest request;

  @Override
  public int cloudConfigSave(JSONObject navigate) {
    Object obj = jedis.get("ForumUser::UserToken::" + request.getHeader(HttpHeaders.AUTHORIZATION));
    if (obj == null) {
      throw OpenApiException.userException(UserErrorEnums.USER_TOKEN_VALID);
    }
    JSONObject tokenJson = (JSONObject) obj;
    if (!navigate.isEmpty()) {
      String username = tokenJson.getString("username"), refer = navigate.getString("refer");
      if (StringUtils.isBlank(refer)) {
        throw OpenApiException.userException(UserErrorEnums.USER_REFER_VALID);
      }
      // 查找用户名作用域下的配置
      ApiUserEntity apiUserEntity =
          findOne(
                  mapper
                      .query()
                      .selectAll()
                      .where
                      .user()
                      .eq(username)
                      .and
                      .reference()
                      .eq(refer)
                      .end())
              .orElse(new ApiUserEntity());
      if (apiUserEntity.empty()) {
        apiUserEntity.setUser(username);
        apiUserEntity.setSettings(navigate.toJSONString());
        apiUserEntity.setReference(navigate.getString("refer"));
        mapper.insert(apiUserEntity);
      } else {
        apiUserEntity.setSettings(navigate.toJSONString());
        updateById(apiUserEntity);
      }
      jedis.remove(CLOUD_CONFIG_CACHE_KEY + username);
      return 1;
    }
    return 0;
  }

  @Override
  public JSONObject getCloudConfig(String refer, String username) {
    Object obj = jedis.get(CLOUD_CONFIG_CACHE_KEY + username);
    JSONObject config = new JSONObject();
    if (obj == null && StringUtils.isNotBlank(username) && StringUtils.isNotBlank(refer)) {
      ApiUserEntity one =
          findOne(
                  mapper
                      .query()
                      .selectAll()
                      .where
                      .enable()
                      .isTrue()
                      .and
                      .user()
                      .eq(username)
                      .and
                      .reference()
                      .eq(refer)
                      .end())
              .orElse(new ApiUserEntity());
      if (!one.empty()) {
        config = JSONObject.parseObject(one.getSettings());
        if (!config.isEmpty()) {
          jedis.set(CLOUD_CONFIG_CACHE_KEY + username, config);
        }
      }
      return config;
    }
    return (JSONObject) obj;
  }
}
