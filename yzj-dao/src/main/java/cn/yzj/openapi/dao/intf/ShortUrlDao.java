package cn.yzj.openapi.dao.intf;

import cn.org.atool.fluent.mybatis.base.IBaseDao;
import cn.yzj.openapi.dto.UrlDTO;
import cn.yzj.openapi.entity.ShortUrlEntity;

/**
 * ShortUrlDao: 数据操作接口
 *
 * <p>这只是一个减少手工创建的模板文件 可以任意添加方法和实现, 更改作者和重定义类名
 *
 * @author gzkemays
 */
public interface ShortUrlDao extends IBaseDao<ShortUrlEntity> {
  /**
   * 生成短链接
   *
   * @param dto 短链接接口入参
   * @return 返回短链接
   */
  String generateShortUrl(UrlDTO dto);

  /**
   * 获取源地址
   *
   * @param shortUrl 地址
   * @return 源地址
   */
  void getOriginUrl(String shortUrl);
}
