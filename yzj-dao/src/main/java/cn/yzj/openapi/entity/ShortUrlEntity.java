package cn.yzj.openapi.entity;

import cn.org.atool.fluent.mybatis.annotation.FluentMybatis;
import cn.org.atool.fluent.mybatis.annotation.TableField;
import cn.org.atool.fluent.mybatis.annotation.TableId;
import cn.org.atool.fluent.mybatis.base.RichEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * ShortUrlEntity: 数据映射实体定义
 *
 * @author Powered By Fluent Mybatis
 */
@SuppressWarnings({"rawtypes", "unchecked"})
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@FluentMybatis(table = "short_url", schema = "yzj_open_api")
public class ShortUrlEntity extends RichEntity {
  private static final long serialVersionUID = 1L;
  /** 原地址 */
  @TableId("id")
  private Integer id;
  /** 定向的地址 */
  @TableField("origin_url")
  private String originUrl;
  /** 短链接 */
  @TableField("short_url")
  private String shortUrl;
  /** 加密key值 */
  @TableField("key")
  private String key;
  /** 加密盐值 */
  @TableField("salt")
  private String salt;
  /** 过期时间 */
  @TableField("expire_time")
  private Date expireTime;
  /** 创建时间 */
  @TableField("create_time")
  private Date createTime;
  /** 更新时间 */
  @TableField("update_time")
  private Date updateTime;
  /** 是否可用 */
  @TableField("enabled")
  private Boolean enabled;

  @Override
  public final Class entityClass() {
    return ShortUrlEntity.class;
  }
}
