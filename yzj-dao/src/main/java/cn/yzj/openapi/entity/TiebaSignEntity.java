package cn.yzj.openapi.entity;

import cn.org.atool.fluent.mybatis.annotation.FluentMybatis;
import cn.org.atool.fluent.mybatis.annotation.TableField;
import cn.org.atool.fluent.mybatis.annotation.TableId;
import cn.org.atool.fluent.mybatis.base.RichEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

/**
 * TiebaSignEntity: 数据映射实体定义
 *
 * @author Powered By Fluent Mybatis
 */
@SuppressWarnings({"rawtypes", "unchecked"})
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@FluentMybatis(table = "tieba_sign", schema = "yzj_open_api")
public class TiebaSignEntity extends RichEntity {
  private static final long serialVersionUID = 1L;

  /** */
  @TableId(value = "id", auto = false)
  private String id;

  /** cookie */
  @TableField("cookie")
  private String cookie;

  /** */
  @TableField("create_time")
  private Date createTime;

  /** */
  @TableField("enabled")
  private Boolean enabled;

  /** 贴吧关注列表 */
  @TableField("follow_list")
  private String followList;

  /** */
  @TableField("update_time")
  private Date updateTime;

  /** 用户名 */
  @TableField("username")
  private String username;
  /** 描述关闭的原因 */
  @TableField("reason")
  private String reason;

  @Override
  public final Class entityClass() {
    return TiebaSignEntity.class;
  }
}
