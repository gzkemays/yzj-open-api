package cn.yzj.openapi.entity;

import cn.org.atool.fluent.mybatis.annotation.FluentMybatis;
import cn.org.atool.fluent.mybatis.annotation.TableField;
import cn.org.atool.fluent.mybatis.annotation.TableId;
import cn.org.atool.fluent.mybatis.base.RichEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * ApiSettingsEntity: 数据映射实体定义
 *
 * @author Powered By Fluent Mybatis
 */
@SuppressWarnings({"rawtypes", "unchecked"})
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@FluentMybatis(table = "api_settings", schema = "yzj_open_api")
public class ApiSettingsEntity extends RichEntity {
  private static final long serialVersionUID = 1L;

  /** */
  @TableId(value = "id", auto = false)
  private String id;

  /** 配置参数 */
  @TableField("settings")
  private String settings;

  @Override
  public final Class entityClass() {
    return ApiSettingsEntity.class;
  }
}
