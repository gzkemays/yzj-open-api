package cn.yzj.openapi.entity;

import cn.org.atool.fluent.mybatis.annotation.FluentMybatis;
import cn.org.atool.fluent.mybatis.annotation.TableField;
import cn.org.atool.fluent.mybatis.annotation.TableId;
import cn.org.atool.fluent.mybatis.base.RichEntity;
import java.util.Date;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * DomainIconEntity: 数据映射实体定义
 *
 * @author Powered By Fluent Mybatis
 */
@SuppressWarnings({"rawtypes", "unchecked"})
@Data
@Accessors(
    chain = true
)
@EqualsAndHashCode(
    callSuper = false
)
@FluentMybatis(
    table = "domain_icon",
    schema = "yzj_open_api"
)
public class DomainIconEntity extends RichEntity {
  private static final long serialVersionUID = 1L;

  /**
   */
  @TableId(
      value = "id",
      auto = false
  )
  private Integer id;

  /**
   * 别名
   */
  @TableField("alias")
  private String alias;

  /**
   */
  @TableField("createTime")
  private Date createTime;

  /**
   * 访问地址
   */
  @TableField("domain")
  private String domain;

  /**
   * 图标地址
   */
  @TableField("icon")
  private String icon;

  /**
   */
  @TableField("updateTime")
  private Date updateTime;

  @Override
  public final Class entityClass() {
    return DomainIconEntity.class;
  }
}
