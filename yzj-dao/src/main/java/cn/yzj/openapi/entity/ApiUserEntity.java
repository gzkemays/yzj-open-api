package cn.yzj.openapi.entity;

import cn.org.atool.fluent.mybatis.annotation.FluentMybatis;
import cn.org.atool.fluent.mybatis.annotation.TableField;
import cn.org.atool.fluent.mybatis.annotation.TableId;
import cn.org.atool.fluent.mybatis.base.RichEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.StringUtils;

/**
 * ApiUserEntity: 数据映射实体定义
 *
 * @author Powered By Fluent Mybatis
 */
@SuppressWarnings({"rawtypes", "unchecked"})
@Data
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = false)
@FluentMybatis(table = "api_user", schema = "yzj_open_api")
public class ApiUserEntity extends RichEntity {
  private static final long serialVersionUID = 1L;

  /** */
  @TableId(value = "id", auto = false)
  private Integer id;

  /** 是否启用 */
  @TableField("enable")
  private Boolean enable;

  /** 来源 */
  @TableField("reference")
  private String reference;

  /** 配置 */
  @TableField("settings")
  private String settings;

  /** 用户名 */
  @TableField("user")
  private String user;

  public boolean empty() {
    return StringUtils.isBlank(user) || id == null || id == 0;
  }

  @Override
  public final Class entityClass() {
    return ApiUserEntity.class;
  }
}
