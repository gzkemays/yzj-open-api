import cn.yzj.utils.JwtUtils;

/**
 * @author gzkemays
 * @since 2022/3/27 10:02
 */
public class Tests {
  public static void main(String[] args) {
    System.out.println(
        JwtUtils.builder()
            .build()
            .checkJwt(
                "eyJhbGciOiJIUzI1NiJ9.eyJsZXZlbCI6IjAiLCJjcmVkZW50aWFsc05vbkV4cGlyZWQiOnRydWUsInVwZGF0ZVRpbWUiOjE2NTQwNjU2ODMwMDAsImF2YXRhciI6Imh0dHBzOi8vaW1nLnllemlqaS54eXovdXNlci9hdmF0YXIvNjc3MmYxNGEyMDQ3MzZjMmQ4YzU1YmJlMWRjMzY4NjkuanBnIiwiZGVsZXRlIjpmYWxzZSwiZW5hYmxlZCI6dHJ1ZSwicGFzc3dvcmQiOiJzcTJsQmxBOUk2Z3B4RENPQ0FzK1J3PT0iLCJjcmVhdGVUaW1lIjoxNjUwMzU3NDk3MDAwLCJuaWNrbmFtZSI6IuS9oOivtOi_h-eDn-iKseS5n-WPq-S9nOeBq-iKsSIsImFjY291bnROb25FeHBpcmVkIjp0cnVlLCJpZCI6MTksImVtYWlsIjoiMTUzODIxMTI4MUBxcS5jb20iLCJkZXNjIjoi5aW95aW95Yqq5Yqb77yM5LiL5aW95q-P5LiA6aG_6aWt44CCIiwidXNlcm5hbWUiOiJnemtlbWF5cyIsImFjY291bnROb25Mb2NrZWQiOnRydWV9.pO-gccTkCKfhn0v8N_FDzTF93TqtJ2Pqn9zSwEvFHfo"));
  }
}
